PowerUp = Class{}


function PowerUp:init(power)

    self.x=-16
    self.y=-16

    self.dy=0
    self.height=16
    self.width=16

    self.active=false
    self.collected=false
    
    self.power=power
end


function PowerUp:update(dt)

self.y = self.y+self.dy*dt

if self.y >=VIRTUAL_HEIGHT then
    self.active=false
end

end

function PowerUp:collides(target)

    if self.x > target.x + target.width or target.x > self.x + self.width then
        return false
    end

    -- then check to see if the bottom edge of either is higher than the top
    -- edge of the other
    if self.y > target.y + target.height or target.y > self.y + self.height then
        return false
    end 

    -- if the above aren't true, they're overlapping
    return true
end

function PowerUp:render()
    
    if self.active then
        love.graphics.draw(gTextures['main'],gFrames['power-ups'][self.power],self.x,self.y)
    end
end